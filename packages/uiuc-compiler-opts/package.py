##############################################################################
# Copyright (c) 2013-2018, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/spack/spack
# Please also see the NOTICE and LICENSE files for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
# -----------------------------------------------------------------------------
# Package file author:  Timothy A. Smith
# -----------------------------------------------------------------------------
from spack import *


class UiucCompilerOpts(CMakePackage):
    """Supplies compiler optimizations for ICE."""

    homepage = "https://bitbucket.org/thiagotei/uiuc-compiler-opts/wiki/Home"
    git      = "git@bitbucket.org:thiagotei/uiuc-compiler-opts.git"

    version("1.1.2", tag="v1.1.2")

    variant('rosefork', default=False, description='Provides some bugfixes for the Rose compiler optimizations')
    depends_on("rose@0.9.9.57:", when="~rosefork")
    depends_on("rosefork@rebNov18", when="+rosefork")

    def cmake_args(self):
        spec = self.spec
        jre_path = spec['java'].prefix + "/jre/lib/amd64/server"
        if '+rosefork' in spec:
            rose_path = spec['rosefork'].prefix
        else:
            rose_path = spec['rose'].prefix
        return [
                "-DBOOST_ROOT={}".format(spec['boost'].prefix),
                "-DROSE_PATH={}".format(rose_path),
                "-DJRE_ROOT={}".format(jre_path)
                ]
