##############################################################################
# Copyright (c) 2013-2018, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/spack/spack
# Please also see the NOTICE and LICENSE files for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *


class Plascom2(CMakePackage):
    """PlasCom2"""

    homepage = "https://bitbucket.org/xpacc-dev/plascom2"
    url      = "https://bitbucket.org/xpacc-dev/plascom2"

    version("develop", git="git@bitbucket.org:xpacc-dev/plascom2", submodules=True)
    version("protoy5", git="git@bitbucket.org:xpacc-dev/plascom2", submodules=True, branch='protoy5')

    depends_on('mpi@2')
    depends_on('hdf5+mpi')

    variant('omp', default=False)
    variant('static', default=False)
    variant('overkit', default=False)
    variant('cstools', default=False)


    def cmake_args(self):
        spec = self.spec
        args = []
        args.extend(['-DENABLE_OPENMP={0}'.format(spec.variants['omp'].value)])
        args.extend(['-DBUILD_STATIC={0}'.format(spec.variants['static'].value)])
        args.extend(['-DENABLE_OVERKIT={0}'.format(spec.variants['overkit'].value)])
        args.extend(['-DCS-TOOLS={0}'.format(spec.variants['cstools'].value)])
        return args
