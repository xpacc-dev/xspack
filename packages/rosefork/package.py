##############################################################################
# Copyright (c) 2013-2018, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/spack/spack
# Please also see the NOTICE and LICENSE files for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
# -----------------------------------------------------------------------------
# Author: Justin Too <too1@llnl.gov>
#
# Edited by Timothy A. Smith <tasmith4@illinois.edu>
# -----------------------------------------------------------------------------

from spack import *


class Rosefork(AutotoolsPackage):
    """A compiler infrastructure to build source-to-source program
       transformation and analysis tools.
       (Developed at Lawrence Livermore National Lab)"""

    homepage = "http://rosecompiler.org/"
    git      = "https://github.com/thiagotei/rose.git"

    version('master', branch='master')
    # TODO: update with actual Rose version this fork is based on
    #version('0.9.10.54', commit='e11cc2d5cb88bc196f5c80421699132c6843bda0')
    version("rebNov18",branch='rebNov18')

    patch('openjdk_version_id.patch')

    depends_on("autoconf@2.59:", type='build')
    depends_on("automake@1.11.1:", type='build')
    depends_on("libtool@1.5.6:", type='build')
    depends_on("flex@2.5.35:")
    depends_on("bison@2.4.1:")
    depends_on("boost@1.51:1.53,1.55:1.61")

    variant('tests', default=False, description='Build the tests directory')

    variant('binanalysis', default=False, description='Enable binary analysis tooling')
    depends_on('libgcrypt', when='+binanalysis', type='build')
    depends_on('py-binwalk', when='+binanalysis', type='run')

    variant('c', default=True, description='Enable c language support')
    variant('cxx', default=True, description='Enable c++ language support')

    variant('fortran', default=True, description='Enable fortran language support')
    depends_on('java@6:9', type=('build', 'run'), when='+fortran')

    variant('java', default=False, description='Enable java language support')
    depends_on('java@6:9', type=('build', 'run'), when='+java')

    variant('z3', default=False, description='Enable z3 theorem prover')
    depends_on('z3', when='+z3')

    build_directory = 'spack-build'

    def autoreconf(self, spec, prefix):
        bash = which('bash')
        bash('build')

        # libharu's config.guess needs to be updated for PowerPC
        am_ver = ".".join(str(self.spec['automake'].version).split('.')[0:2])
        am_version = 'automake-' + am_ver
        am_path = join_path(self.spec['automake'].prefix.share, am_version)
        copy(join_path(am_path, 'config.guess'), 'src/3rdPartyLibraries/libharu-2.1.0')

    @property
    def languages(self):
        spec = self.spec
        langs = [
            'binaries' if '+binanalysis' in spec else '',
            'c' if '+c' in spec else '',
            'c++' if '+cxx' in spec else '',
            'java' if '+java' in spec else '',
            'fortran' if '+fortran' in spec else ''
        ]
        return list(filter(None, langs))

    def configure_args(self):
        spec = self.spec
        return [
            '--disable-boost-version-check',
            "--with-alternate_backend_C_compiler={0}".format(self.compiler.cc),
            "--with-alternate_backend_Cxx_compiler={0}".format(self.compiler.cxx),
            "--with-alternate_backend_fortran_compiler={0}".format(self.compiler.fc),
            "--with-gfortran={0}".format(self.compiler.fc),
            "--with-boost={0}".format(spec['boost'].prefix),
            "--enable-languages={0}".format(",".join(self.languages)),
            "--with-z3={0}".format(spec['z3'].prefix) if '+z3' in spec else '',
            '--disable-tests-directory' if '+tests' not in spec else '',
            '--disable-tutorial-directory',
        ]

    install_targets = ["install-core"]
