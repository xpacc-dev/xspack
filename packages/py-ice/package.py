##############################################################################
# Copyright (c) 2013-2018, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/spack/spack
# Please also see the NOTICE and LICENSE files for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
#
from spack import *


class PyIce(PythonPackage):
    """ICE (Illinois Coding Environment) is the pre-processing and optimization tool
       developed under XPACC. It's role is optimization search space, and code transformation
       to utilize specific optimized tunings."""

    homepage = "https://bitbucket.org/xpacc-dev/ice/wiki/Home"
    git      = "git@bitbucket.org:xpacc-dev/ice.git"

    version('1.4.0', tag='v1.4.0')
    #version('1.3.3', tag='v1.3.3')
    #version('1.1.4', tag='v1.1.4')
    #version('1.1.3', tag='v1.1.3')
    #version('1.1', tag='v1.1.0')

    # FIXME: Add dependencies if required.
    depends_on('python@3.6:', type=('build', 'run'))
    depends_on('py-setuptools', type='build')
    #depends_on('py-pyyaml',     type=('build', 'run'))
    depends_on('py-opentuner@0.8.2:', type=('build', 'run'))
    depends_on('py-pycparser', type=('build', 'run'))
    depends_on('py-lark-parser', type=('build', 'run'))

    # Python 2 requirements
    #depends_on('py-subprocess32@3.2.7')
    #depends_on('py-numpy@:1.16.999')

    variant('rose', default=False, description='Provides the Rose compiler optimizations')
    depends_on('uiuc-compiler-opts@1.1.2:', when='+rose', type='run')

    variant('rosefork', default=False, description='Provides some bugfixes for the Rose compiler optimizations')
    depends_on('uiuc-compiler-opts@1.1.2:+rosefork%gcc@4.4.0:5.2.99', when='+rosefork', type='run')

    def setup_environment(self, spack_env, run_env):
        spec = self.spec
        if '+rose' in spec or '+rosefork' in spec:
            run_env.set('ICE_CMPOPTS_PATH', spec['uiuc-compiler-opts'].prefix)
